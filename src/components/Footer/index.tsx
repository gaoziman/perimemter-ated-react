import { GithubOutlined } from '@ant-design/icons';
import { DefaultFooter } from '@ant-design/pro-components';
import '@umijs/max';
import React from 'react';

const Footer: React.FC = () => {
  const defaultMessage = '程序员Leo';
  const currentYear = new Date().getFullYear();
  return (
    <DefaultFooter
      style={{
        background: 'none',
      }}
      copyright={`${currentYear} ${defaultMessage}`}
      links={[
        {
          key: 'codeNav',
          title: '知识宝库',
          href: 'https://gaoziman.github.io/toLeoJavaer/',
          blankTarget: true,
        },
        {
          key: 'Ant Design',
          title: '博客论坛',
          href: 'https://gaoziman.blog.csdn.net/',
          blankTarget: true,
        },
        {
          key: 'github',
          title: (
            <>
              <GithubOutlined /> Leo哥
            </>
          ),
          href: 'https://github.com/gaoziman',
          blankTarget: true,
        },
      ]}
    />
  );
};
export default Footer;
