# leocoder前端通用模版


基于 React + Ant Design 的项目初始模板，整合了常用框架和主流业务的示例代码。


## 模板特点

### 主流框架 & 特性

+ Ant Design Pro 6.0.0
+ React 18.2.0
+ node 至少 16 版本及以上
+ antd 5.2.2
+ Type Script
+ 动态路由
+ Eslint
+ Prettier

### Ant Design Pro 架构


### 业务特性

+ 栅格布局（可自定义，可适应）
+ 简单权限管理 
+ 全局初始数据（ getInitialState )
+ 默认使用 less 作为样式语言
+ OpenAPI 自动生成后端请求代码
+ 统一错误处理

## 业务功能

+ 提供 OpenAPI 后端接口自动生成
+ 用户登录、用户注册
+ 管理员修改用户、新建用户、查询用户、删除用户
+ 动态路由展示（权限管理）


## 技术交流



## 学习测试




## 测试测试

